# Tailwind CSS experiment

## Disclaimer

This is an experimental theme under heavy development, use with caution.

## Commands

Install dependencies
```shell script
npm install
```

The following commands also exist with the `:dev` suffix providing a more verbose output and less post-processing.

Build assets
```shell script
npm run build
```

Build assets and watch
```shell script
npm run watch
```

Launch local server
```shell script
npm run serve
```
