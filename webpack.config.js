const glob = require("glob");
const path = require("path");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const StylelintPlugin = require("stylelint-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const WatchExternalFilesPlugin = require("webpack-watch-files-plugin").default;
const WebpackBar = require("webpackbar");

module.exports = (env, argv) => {
  const isDevelopment = argv.mode === "development";

  return {
    mode: isDevelopment ? "development" : "production",

    devtool: isDevelopment ? "source-map" : false,

    devServer: {
      compress: true,
      contentBase: ["./templates"],
      watchContentBase: true,
      proxy: {
        context: "/",
        target: "http://tailwindcss-experiment.docker.test",
        secure: false,
        changeOrigin: true,
      },
      writeToDisk: true,
    },

    entry: {
      main: glob.sync("./src/js/**/*.js").concat("./src/css/main.css"),
    },

    module: {
      rules: [
        {
          test: /\.css$/,
          use: [
            {
              loader: MiniCssExtractPlugin.loader,
            },
            {
              loader: "css-loader",
              options: {
                sourceMap: isDevelopment,
                modules: false,
              },
            },
            {
              loader: "postcss-loader",
              options: {
                sourceMap: isDevelopment,
              },
            },
          ],
        },
        {
          test: /\.js$/,
          enforce: "pre",
          exclude: /(node_modules|bower_components|dist)/,
          loader: "eslint-loader",
          options: {
            fix: true,
          },
        },
        {
          test: /\.js$/,
          exclude: /(node_modules|bower_components|dist)/,
          loader: "babel-loader",
          options: {
            presets: [["@babel/preset-env", { modules: false }]],
          },
        },
        {
          test: /\.(png|svg|jpg|gif)$/,
          use: [
            {
              loader: "file-loader",
              options: {
                name: "[path][name].[ext]",
                context: "src",
              },
            },
            {
              loader: "image-webpack-loader",
              options: {
                disable: isDevelopment,
                mozjpeg: {
                  progressive: true,
                  quality: 90,
                },
                pngquant: {
                  quality: [0.75, 0.9],
                },
                webp: {
                  quality: 90,
                },
              },
            },
          ],
        },
        {
          test: /\.(woff|woff2|eot|ttf|otf)$/,
          use: [
            {
              loader: "file-loader",
              options: {
                name: "fonts/[name].[ext]",
              },
            },
          ],
        },
      ],
    },

    optimization: {
      minimize: !isDevelopment,
      minimizer: [
        new TerserPlugin({
          extractComments: false,
        }),
        new OptimizeCSSAssetsPlugin(),
      ],
      splitChunks: {
        cacheGroups: {
          vendor: {
            test: /node_modules/,
            chunks: "initial",
            name: "vendor",
            enforce: true,
          },
        },
      },
    },

    output: {
      path: path.resolve(__dirname, "dist"),
      filename: "js/[name].js",
      publicPath: "/themes/custom/tailwindcss_experiment/dist/",
    },

    plugins: [
      new WebpackBar({
        name: `Tailwind CSS Experiment (${argv.mode})`,
        color: isDevelopment ? "yellow" : "green",
      }),
      new CleanWebpackPlugin(),
      new MiniCssExtractPlugin({
        filename: "css/[name].css",
      }),
      new StylelintPlugin({
        context: "./src",
        files: "**/*.css",
        fix: true,
      }),
      new WatchExternalFilesPlugin({
        files: ["./templates/**/*.twig"],
      }),
    ],

    stats: {
      chunks: isDevelopment,
      entrypoints: isDevelopment,
      logging: isDevelopment ? true : "warn",
      modules: isDevelopment,
      version: isDevelopment,
    },
  };
};
